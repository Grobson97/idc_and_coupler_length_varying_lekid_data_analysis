import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model
import skrf as rf
import util.lekid_analysis_tools
from scipy import stats


def get_port_parameters(string: str):
    """
    Function to extract port variables from comments in touchstone data.

    :param string: String for a given line in a comment in a touchstone file.
    :return:
    """
    pos_in_line = 0
    port_dictionary = {}

    while pos_in_line < len(string):

        # Determine index of next equals sign.
        equals_pos = string.find("=", pos_in_line, len(string))
        # If no more equals signs, break loop.
        if equals_pos == -1:
            break

        # Read back from equals sign to extract var_name
        var_name_start_pos = string.rfind(" ", pos_in_line, equals_pos)
        var_name = string[var_name_start_pos:equals_pos].strip()

        # If statement to extract value corresponding to var_name
        # If single values
        if string[equals_pos + 1] != "(":
            var_end_pos = string.find(" ", equals_pos, len(string))
            var = string[equals_pos + 1 : var_end_pos].strip()
        # If complex, i.e in a bracket.
        else:
            var_end_pos = string.find(") ", equals_pos, len(string))
            split_pos = string.find("+", equals_pos, var_end_pos)
            if split_pos == -1:
                split_pos = string.find("-", equals_pos, var_end_pos)
            var_real = float(string[equals_pos + 2 : split_pos].strip())  # First value
            var_imag = float(
                string[split_pos + 3 : var_end_pos].strip(" )")
            )  # Second value
            var = complex(var_real, var_imag)

        # Add var_name and var as key name and value to port dictionary.
        port_dictionary[var_name] = var
        # Update pos_in_line to end of var.
        pos_in_line = var_end_pos

    return port_dictionary


def get_mean_port_impedance(file_path: str, port_number: str) -> list:
    """
    Function to return the mean impedance of a given port across a frequency range from a touchstone file.
    Impedance returned in list

    :param file_path: Path to data file
    :param port_number: Port number to determine the mean impedance for.
    :return: List of impedance values in format: [r_mean, x_mean, Rerr, Xerr]
    """

    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        impedance_data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                impedance_data_points += 1

    with open(file_path, "r") as reader:
        port_impedances = {
            "F": np.empty(shape=impedance_data_points),
            "Z0": np.empty(shape=impedance_data_points, dtype=complex),
        }
        data_point = 0
        # If line starts with '!< PN' where N is the port_number, extract the dictionary of parameters then
        # collect frequency and corresponding Z0 values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_impedances["F"][data_point] = current_port_params["F"]
                port_impedances["Z0"][data_point] = current_port_params["Z0"]

                data_point += 1

        r_mean = np.mean(port_impedances["Z0"].real)
        r_err = (
            abs(np.max(port_impedances["Z0"].real) - np.min(port_impedances["Z0"].real))
            / 2
        )
        x_mean = np.mean(port_impedances["Z0"].imag)
        x_err = (
            abs(np.max(port_impedances["Z0"].imag) - np.min(port_impedances["Z0"].imag))
            / 2
        )

    return [r_mean, x_mean, r_err, x_err]


def get_port_z0_array(file_path, port_number: str) -> np.ndarray:
    """
    Function to return an array of port Z0 values extracted from an output
    touchstone file of a sonnet simulation. These Z0 values correspond to the
    port impedance at a certain frequency. The corresponding frequencies can be
    extracted using the get_port_f_array() method.

    port_number is a string of an integer.
    """
    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                data_points += 1

    with open(file_path, "r") as reader:
        port_impedances = np.empty(shape=data_points, dtype=complex)
        data_point = 0
        # If line starts with '!< PN' where N is the port_number, extract the dictionary of parameters then
        # collect frequency and corresponding Z0 values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_impedances[data_point] = current_port_params["Z0"]

                data_point += 1

    return port_impedances


def get_port_f_array(file_path, port_number: str) -> np.ndarray:
    """
    Function to return an array of frequency values at which the port z0 and
    Eeff were evaluated in the sonnet touchstone file.

    :param file_path: Path to data file
    :param port_number: Port number to determine the mean impedance for. String of an integer.
    :return:
    """

    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                data_points += 1

    with open(file_path, "r") as reader:
        port_frequencies = np.empty(shape=data_points, dtype=float)
        data_point = 0
        # If line starts with '!< PN' where N is the port_number, extract the dictionary of parameters then
        # collect frequency and corresponding Z0 values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_frequencies[data_point] = current_port_params["F"]

                data_point += 1

    return port_frequencies


def get_port_Eeff_array(file_path: str, port_number: str) -> np.ndarray:
    """
    Function to return an array of port Eeff values extracted from an output
    touchstone file of a sonnet simulation. These Eeff values correspond to the
    port impedance at a certain frequency. The corresponding frequencies can be
    extracted using the get_port_f_array() method.

    port_number is a string of an integer.
    """
    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                data_points += 1

    with open(file_path, "r") as reader:
        port_permittivities = np.empty(shape=data_points, dtype=complex)
        data_point = 0
        # If line starts with '!< PN' where N is the port_number, extract the dictionary of parameters then
        # collect frequency and corresponding Z0 values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_permittivities[data_point] = current_port_params["Eeff"]

                data_point += 1

    return port_permittivities


def make_renormalised_network(data_file_path: str, n_ports: int):
    """
    Function to create a scikit-rf network object from a touchstone file and renormalise the S parameters by the
    mean port impedance over the frequency sweep range.

    :param data_file_path: Path to touchstone data file.
    :param n_ports: number of ports in data file. One port: 1, two ports: 2 etc.
    :return:
    """

    # Create network from touchstone file
    network = rf.Network(data_file_path)

    (
        port_1_r,
        port_1_x,
        port_1_r_err,
        port_1_x_err,
    ) = util.sonnet_simulation_tools.get_mean_port_impedance(data_file_path, "1")

    mean_port_z0_list = [complex(port_1_r, port_1_x)]

    if n_ports > 1:
        (
            port_2_r,
            port_2_x,
            port_2_r_err,
            port_2_x_err,
        ) = util.sonnet_simulation_tools.get_mean_port_impedance(data_file_path, "2")
        mean_port_z0_list.append(complex(port_2_r, port_2_x))

    if n_ports > 2:
        (
            port_3_r,
            port_3_x,
            port_3_r_err,
            port_3_x_err,
        ) = util.sonnet_simulation_tools.get_mean_port_impedance(data_file_path, "3")
        mean_port_z0_list.append(complex(port_3_r, port_3_x))

    if n_ports > 3:
        (
            port_4_r,
            port_4_x,
            port_4_r_err,
            port_4_x_err,
        ) = util.sonnet_simulation_tools.get_mean_port_impedance(data_file_path, "4")
        mean_port_z0_list.append(complex(port_4_r, port_4_x))

    network.renormalize(mean_port_z0_list)

    return network


def plot_network_s_params(
    network: rf.Network,
    from_port: int,
    to_port: int,
    plot_title: "",
    plot_db: True,
    plot_power: True,
) -> None:
    """
    Function to plot the desired scattering parameters of a given scikit-rf network vs frequency in GHz.

    :param network: Network object with scattering parameter arrays.
    :param from_port: Input port number for scattering parameter. Port 1: 1, Port 2: 2.
    :param to_port: output port number for scattering parameter. Port 1: 1, Port 2: 2.
    :param plot_title: Title to give the plot, default is an empty string.
    :param plot_db: Boolean to plot S param in decibels.
    :param plot_power: Boolean to plot S param in power (magnitude squared). Note if plot_db must be False, otherwise
    the plot will be in dB. If both are false, S param will be plotted in absolute magnitude.
    :return:
    """
    # Change from and to port numbers to the corresponding index
    from_port = from_port - 1
    to_port = to_port - 1

    if plot_db:
        s_param = network.s_db[:, from_port, to_port]
        y_label = "S%i%i Magnitude (dB)" % (from_port + 1, to_port + 1)
    elif plot_power:
        s_param = np.absolute(network.s[:, from_port, to_port]) ** 2
        y_label = "S%i%i Power" % (from_port + 1, to_port + 1)
    else:
        s_param = np.absolute(network.s[:, from_port, to_port])
        y_label = "S%i%i Magnitude" % (to_port + 1, from_port + 1)

    plt.figure(num=1, figsize=(8, 6))
    plt.plot(network.f * 1e-9, s_param, linestyle="-")
    plt.xlabel("Frequency (GHz)")
    plt.ylabel(y_label)
    plt.legend(loc="center left", bbox_to_anchor=(0.0, 1.2))
    plt.title(plot_title)
    plt.tight_layout()
    plt.show()

    return None


def fit_linear_regression(
    x_data: np.ndarray,
    y_data: np.ndarray,
    plot_graph=True,
    plot_title="",
    x_label="X Data",
    y_label="Y Data",
):
    """
    Function to fit a linear regression to x and y data and return the scipy LinregressResult object. See
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html for attributes and methods.

    :param x_data: X data to fit to.
    :param y_data: Y data to fit to.
    :param plot_graph: Boolean to
    :param x_label: Label for x axis of plot.
    :param y_label: Label for y axis of plot.
    :return: result: LinregressResult instance. The return value is an object with the following attributes:
        slope: float
            Slope of the regression line.
        intercept: float
            Intercept of the regression line.
        rvalue: float
            The Pearson correlation coefficient. The square of ``rvalue``
            is equal to the coefficient of determination.
        pvalue: float
            The p-value for a hypothesis test whose null hypothesis is
            that the slope is zero, using Wald Test with t-distribution of
            the test statistic. See `alternative` above for alternative
            hypotheses.
        stderr: float
            Standard error of the estimated slope (gradient), under the
            assumption of residual normality.
        intercept_stderr: float
            Standard error of the estimated intercept, under the assumption
            of residual normality.
    """

    result = stats.linregress(x_data, y_data)

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", label="Data")
        plt.plot(
            x_data,
            result.intercept + result.slope * x_data,
            color="r",
            label="y = mx + c fit with:\nm=%.2e\nc=%.2e\nR$^2$=%.2e\n$\sigma_m$=%.2e\n$\sigma_c$=%.2e"
            % (
                result.slope,
                result.intercept,
                result.rvalue**2,
                result.stderr,
                result.intercept_stderr,
            ),
        )
        plt.title(plot_title)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.legend()
        plt.show()

    return result
