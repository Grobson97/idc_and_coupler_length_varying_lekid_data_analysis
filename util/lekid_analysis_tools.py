import matplotlib.pyplot as plt
import numpy as np
from lmfit import Model
from lmfit import Parameters


def lorentzian_s21(
    frequency_array: np.array, Qr: float, Qc: float, f0: float, A=1.0
) -> np.array:
    """
    Function defining the model of a lorentzian resonance in an S21 array.

    :param np.array frequency_array: Array of N frequency points.
    :param float Qr: Resonator quality factor.
    :param float Qc: Coupling quality factor.
    :param float f0: Resonant frequency.
    :param float A: Amplitude/ background level.
    """
    x = (frequency_array - f0) / f0
    a = 2.0 * Qr * x
    real_s21 = A - (Qr / Qc) * (1.0 / (a**2 + 1.0))
    imaginary_s21 = (Qr / Qc) * (a / (a**2 + 1.0))
    magnitude_s21 = np.sqrt(real_s21**2 + imaginary_s21**2)
    return magnitude_s21


def fit_lorentzian_s21(
    frequency_array: np.array,
    s21_array: np.array,
    Qr_guess: float,
    Qc_guess: float,
    plot_graph=True,
    plot_dB=True,
    plot_title="",
) -> list:
    """
    Function to fit qr, Qc and f0 to a resonance in S21.\n
    Returns fit parameters as a list: [qr, Qc, f0, a].

    :param np.array frequency_array: Array of N frequency points.
    :param np.array s21_array: Complex array of N S21 data points.
    :param float Qr_guess: Guess of resonators quality factor.
    :param float qr_guess: Guess of coupling quality factor.
    :param bool plot_graph: Boolean command to plot a graph of the fit.
    :param bool plot_dB: Boolean command to plot S21 in dB.

    """

    s21_mag_array = np.abs(s21_array)

    f0_index = np.argmin(s21_mag_array)  # index of minimum point.
    resonance_data_points = (
        300  # define number of data points either side of resonance to sample
    )
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size:
        end_index = -1

    # Define model:
    l_model = Model(lorentzian_s21)

    # Define Parameters:
    params = Parameters()
    params.add("Qc", value=Qc_guess, min=0, vary=True)
    params.add("qr", value=Qr_guess, expr="Qc")
    params.add(
        "f0", value=frequency_array[np.argmin(s21_mag_array)], min=0, vary=True
    )  # Guesses f0 is min of s21
    params.add(
        "a", value=np.mean(np.abs(s21_array)), vary=True
    )  # Guesses background level as mean of s21.

    result = l_model.fit(
        data=np.abs(s21_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    best_fit_parameters = [
        result.best_values["qr"],
        result.best_values["Qc"],
        result.best_values["f0"],
        result.best_values["a"],
    ]

    if plot_graph == True:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=100000,
        )

        lorentzian_fit = lorentzian_s21(
            frequency_array=f_array,
            Qr=result.best_values["qr"],
            Qc=result.best_values["Qc"],
            f0=result.best_values["f0"],
            A=result.best_values["a"],
        )

        if plot_dB == True:
            y_label = "S21 Magnitude"
            s21_mag_array = 20 * np.log10(s21_mag_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        if plot_dB == False:
            y_label = "S21 Magnitude (dB)"

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            s21_mag_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nqr=%0.2e\nQc=%0.2e\nf0=%0.2e"
            % (
                result.best_values["qr"],
                result.best_values["Qc"],
                result.best_values["f0"],
            ),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    return best_fit_parameters


def simple_lorentzian(
    frequency_array: np.array, I: float, bw: float, f0: float, c: float
) -> np.array:
    """
    Function defining a very simple model of a lorentzian.
    Returns

    :param np.array frequency_array: Array of N frequency points.
    :param float a: Amplitude.
    :param float bw: Full width half maximum.
    :param float f0: Resonant frequency.
    :param float c: Offset in y.
    """
    x = (frequency_array - f0) / (0.5 * bw)
    lorentzian = I / (1 + x**2) + c
    return lorentzian


def fit_simple_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    bw_guess: np.array,
    plot_graph=True,
    plot_dB=True,
    plot_title="",
) -> list:
    """
    Function to fit a very simple lorentzian bw, f0, I, a to a resonance in S21.\n
    Returns fit parameters as a list: [Q, bw, f0, I, a].

    :param frequency_array: Array of N frequency points.
    :param data_array: Complex array of N S21 data points.
    :param bw_guess: Guess for the bandwidth of the lorentzian.
    :param plot_graph: Boolean command to plot a graph of the fit.
    :param plot_dB: Boolean command to plot S Parameter in dB.

    """

    magnitude_array = np.abs(data_array)

    f0_index = np.argmax(magnitude_array)  # index of minimum point.
    resonance_data_points = (
        500  # define number of data points either side of resonance to sample
    )
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size:
        end_index = -1

    # Define model:
    l_model = Model(simple_lorentzian)

    # Define Parameters:
    params = Parameters()
    params.add("bw", value=bw_guess, vary=True)
    params.add("I", value=np.max(magnitude_array), vary=True)
    params.add("f0", value=frequency_array[np.argmax(magnitude_array)], vary=True)
    # Guesses background level as mean of s21.
    params.add("c", value=np.mean(np.abs(magnitude_array)), vary=True)

    # l_model.fit
    result = l_model.fit(
        data=np.abs(magnitude_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    Q = result.best_values["f0"] / result.best_values["bw"]

    best_fit_parameters = [
        Q,
        result.best_values["bw"],
        result.best_values["f0"],
        result.best_values["I"],
        result.best_values["c"],
    ]

    if plot_graph:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=10000,
        )

        lorentzian_fit = simple_lorentzian(
            frequency_array=f_array,
            I=result.best_values["I"],
            bw=result.best_values["bw"],
            f0=result.best_values["f0"],
            c=result.best_values["c"],
        )

        if plot_dB:
            y_label = "S21 Magnitude"
            magnitude_array = 20 * np.log10(magnitude_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        if not plot_dB:
            y_label = "S21 Magnitude (dB)"

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            magnitude_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nQ=%0.2e" % Q,
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    return best_fit_parameters


def skewed_lorentzian(
    frequency_array: np.ndarray,
    f0: float,
    qr: float,
    qc_real: float,
    qc_imag: float,
    a: float,
) -> np.ndarray:
    """
    Model of a skewed lorentzian using a complex Qc.

    :param frequency_array: Array of N frequency points.
    :param f0: Resonant frequency
    :param qr: Total resonator quality factor
    :param qc_real: Real coupling quality factor
    :param qc_imag: Imaginary coupling quality factor
    :param a: Amplitude
    :return:
    """

    x = (frequency_array - f0) / f0
    return abs(a * (1 - qr / (qc_real + 1j * qc_imag) / (1 + 2j * qr * x)))


def fit_skewed_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    qr_guess: float,
    f0_guess: float or None,
    plot_graph=True,
    plot_dB=True,
    plot_title="",
) -> dict:
    """
    Function to fit a very simple lorentzian bw, f0, I, a to a resonance in S21.\n
    Returns dictionary of fit parameters with standard errors, e.g. result_dict["qr"] = [value, error].

    :param frequency_array: Array of N frequency points.
    :param data_array: Complex array of N S21 data points.
    :param qr_guess: Guess for the qr of the lorentzian.
    :param f0_guess: Guess for the resonant frequency.
    :param plot_graph: Boolean command to plot a graph of the fit.
    :param plot_dB: Boolean command to plot S Parameter in dB.
    :param plot_title: Title to give to plot. Default is an empty string.

    """

    magnitude_array = np.abs(data_array)

    f0_index = np.argmin(magnitude_array)  # index of minimum point.
    resonance_data_points = round(
        len(magnitude_array) / 2
    )  # define number of data points either side of resonance to sample
    start_index = f0_index - round(resonance_data_points)
    end_index = f0_index + round(resonance_data_points)
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size - 1:
        end_index = -1

    # Define model:
    l_model = Model(skewed_lorentzian)

    f0_first_guess = frequency_array[f0_index]
    if f0_guess is not None:
        f0_first_guess = f0_guess

    # Define Parameters:
    params = Parameters()
    params.add("f0", value=f0_first_guess, vary=True)
    params.add("qr", value=qr_guess, vary=True, min=0)
    params.add("qc_real", value=qr_guess * 2, vary=True, min=0)
    params.add("qc_imag", value=100.0, vary=True)
    # Guesses background level as mean of s21.
    params.add("a", value=np.mean(np.abs(magnitude_array)), vary=True)

    # l_model.fit
    result = l_model.fit(
        data=np.abs(magnitude_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    try:
        result_dict["qc"] = [
            abs(result_dict["qc_real"][0] ** 2 + 1j * result_dict["qc_imag"][0])
            / result_dict["qc_real"][0],
            np.sqrt(
                result_dict["qc_real"][1] ** 2
                + (
                    2
                    * result_dict["qc_imag"][1]
                    * result_dict["qc_imag"][0]
                    / result_dict["qc_real"][0]
                )
                ** 2
                + result_dict["qc_real"][1]
                * result_dict["qc_imag"][0] ** 2
                / result_dict["qc_real"][0] ** 2
            ),
        ]
    except TypeError:
        print("Possible error: The fit value for Qc has None type standard errors.")
        result_dict["qc"] = [
            abs(result_dict["qc_real"][0] ** 2 + 1j * result_dict["qc_imag"][0])
            / result_dict["qc_real"][0],
            None,
        ]
    qi = 1 / ((1 / result_dict["qr"][0]) - (1 / result_dict["qc"][0]))
    try:
        result_dict["qi"] = [
            qi,
            np.sqrt(
                qi**4
                * (
                    result_dict["qr"][1] ** 2 / result_dict["qr"][0] ** 4
                    + result_dict["qc"][1] ** 2 / result_dict["qc"][0] ** 4
                )
            ),
        ]
    except TypeError:
        print(
            "Possible error: The fit value for Qr or Qc has None type standard errors."
        )
        result_dict["qi"] = [qi, None]

    f_array = np.linspace(
        start=frequency_array[start_index],
        stop=frequency_array[end_index],
        num=1000000,
    )

    lorentzian_fit = skewed_lorentzian(
        frequency_array=f_array,
        f0=result_dict["f0"][0],
        qr=result_dict["qr"][0],
        qc_real=result_dict["qc_real"][0],
        qc_imag=result_dict["qc_imag"][0],
        a=result_dict["a"][0],
    )

    if plot_graph:

        if plot_dB:
            y_label = "S21 Magnitude (dB)"
            magnitude_array = 20 * np.log10(magnitude_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        if not plot_dB:
            y_label = "S21 Magnitude"

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            magnitude_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nQr=%0.2e\nQc=%0.2e\nQi=%0.2e\nf0=%0.2e"
            % (
                result_dict["qr"][0],
                result_dict["qc"][0],
                result_dict["qi"][0],
                result_dict["f0"][0],
            ),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    result_dict["fit_f_array"] = f_array
    result_dict["fit_array"] = lorentzian_fit

    return result_dict


def fit_and_interpolate_polynomial(
    x_data: np.array, y_data: np.array, xi: float, degree: int, plot_graph: True
):
    """
    Function to fit a polynomial of a specified degree to x and y data and interpolate
    at a given value.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param float xi: x value at which to interpolate a y value.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial = np.poly1d(coefficients)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree " + str(degree),
        )
        plt.vlines(xi, np.min(y_data), np.max(y_data), color="k")
        plt.hlines(polynomial(xi), np.min(x_data), np.max(x_data), color="k")
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    return polynomial(xi)


def fit_polynomial_find_minima(
    x_data: np.array, y_data: np.array, degree: int, plot_graph: True
):
    """
    Function to fit a polynomial of a specified degree to x and y data and locate it's minimum points.
    returns the poly1D polynomial fit object and the minimum points.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial = np.poly1d(coefficients)

    critical_points = polynomial.deriv().r

    # Find real critical points within the x_data bounds:
    valid_critical_points = []
    for x in critical_points:
        if x.imag == 0 and np.min(x_data) < x.real < np.max(x_data):
            valid_critical_points.append(x)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree " + str(degree),
        )
        plt.vlines(
            valid_critical_points,
            np.min(polynomial(valid_critical_points)),
            np.max(y_data),
            color="k",
        )
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    return polynomial, valid_critical_points
