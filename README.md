#### How to use

Ensure all the relevant (and only the relevant) parameter sweep data files are put in a new folder called "data". The parameters that
are swept in sonnet must be called "idc_length" and "coupler_length", or edit the scripts accordingly.

Run the desired script.

 * **plot_single_lekid_s21.py** simply renormalises the S parameters to the port impedances and plots the S21 of a given
LEKID data file which needs to be specified in the script.
 * **plot_and_fit_s21_vs_single_variable.py** Sweeps through the files in "data" and plots the variation in f0 and
quality factors as a function of a variable defined in the script with "variable_name".
 * **plot_multiple_lekid_s21.py** Sweeps through all the data files in the "data" folder, renormalising and plotting
all the resonances on a single plot.
 * **plot_and_fit_single_lekid_s21.py** For a given LEKID data file (needs to be specified in the script), the script
renormalises the S parameters to the port impedances and fits a skewed lorentzian to the S21 then produces a plot of the
fit.
 * **plot_and_fit_multiple_lekid_s21.py** Sweeps through all the data files in the "data" folder, renormalising and
fitting the resonances with a skewed lorentzian with the option to show a plot of the fit. Summary plots are then also
output showing how the Qr, Qc and Qi vary for each parameter combination. Fitting a skewed lorentzian and (if specified
at the start) plotting each one giving the f0, Qr and Qc. The script will also plot a histogram of how the f0's, and Q's 
vary over the parameter sweep.
 * **plot_fit_interpolate_multiple_lekid_s21.py** Sweeps through all the data files, doing the same as the
 * plot_and_fit_multiple_lekid_s21.py script, but also interpolates what coupler_length is required for each idc_length
to achieve the desired Qc. Then it will output a polynomial to be used to interpolate the idc_length and coupler_length
in order to achieve a LEKID that will resonate at a given f0 for the desired Qc. i.e f0 as a function of idc_length and
coupler_length.
 * **lekid_cross_coupling_analysis.py** Sweeps through all files in data directory, normalises s parameters to port 
impedances. Data files must be from a sonnet simulation of two LEKIDs with one at a fixed f0 (f0_1) and and another
sweeping f0_2 from f0_1 - f to f0_1 + f. The length of the IDC's is controlled in sonnet via these f0 values. Ensure
model converting f0 to an IDC length is the same as that used by lekid_idc_length function found in the script.
Script outputs various graphs of the cross coupling behaviour.
