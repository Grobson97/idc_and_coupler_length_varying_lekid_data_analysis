! Summary of output files for parameterized project.
!
! Project: hybrid_idc_lekid.son
! Folder:  C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\FBS_LEKIDs\hybrid_idc_lekid\300um_SiN_parameter_sweep_data
! Date:    09/24/2022 17:50:58

FTYP POFL 1
HDATE 08/09/2022 12:14:32


USER_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand.s2p

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param01.s2p
PARAMS
idc_length = 200.0
coupler_length = 60.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param02.s2p
PARAMS
idc_length = 200.0
coupler_length = 90.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param03.s2p
PARAMS
idc_length = 200.0
coupler_length = 120.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param04.s2p
PARAMS
idc_length = 200.0
coupler_length = 150.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param05.s2p
PARAMS
idc_length = 200.0
coupler_length = 180.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param06.s2p
PARAMS
idc_length = 400.0
coupler_length = 60.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param07.s2p
PARAMS
idc_length = 400.0
coupler_length = 90.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param08.s2p
PARAMS
idc_length = 400.0
coupler_length = 120.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param09.s2p
PARAMS
idc_length = 400.0
coupler_length = 150.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param10.s2p
PARAMS
idc_length = 400.0
coupler_length = 180.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param11.s2p
PARAMS
idc_length = 600.0
coupler_length = 60.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param12.s2p
PARAMS
idc_length = 600.0
coupler_length = 90.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param13.s2p
PARAMS
idc_length = 600.0
coupler_length = 120.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param14.s2p
PARAMS
idc_length = 600.0
coupler_length = 150.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param15.s2p
PARAMS
idc_length = 600.0
coupler_length = 180.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param16.s2p
PARAMS
idc_length = 800.0
coupler_length = 60.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param17.s2p
PARAMS
idc_length = 800.0
coupler_length = 90.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param18.s2p
PARAMS
idc_length = 800.0
coupler_length = 120.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param19.s2p
PARAMS
idc_length = 800.0
coupler_length = 150.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param20.s2p
PARAMS
idc_length = 800.0
coupler_length = 180.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param21.s2p
PARAMS
idc_length = 1000.0
coupler_length = 60.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param22.s2p
PARAMS
idc_length = 1000.0
coupler_length = 90.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param23.s2p
PARAMS
idc_length = 1000.0
coupler_length = 120.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param24.s2p
PARAMS
idc_length = 1000.0
coupler_length = 150.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param25.s2p
PARAMS
idc_length = 1000.0
coupler_length = 180.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param26.s2p
PARAMS
idc_length = 1200.0
coupler_length = 60.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param27.s2p
PARAMS
idc_length = 1200.0
coupler_length = 90.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param28.s2p
PARAMS
idc_length = 1200.0
coupler_length = 120.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param29.s2p
PARAMS
idc_length = 1200.0
coupler_length = 150.0
END PARAMS

OUTPUT_FILE hybrid_idc_lekid_300um_SiN_2e-4_tand_param30.s2p
PARAMS
idc_length = 1200.0
coupler_length = 180.0
END PARAMS

END USER_FILE
