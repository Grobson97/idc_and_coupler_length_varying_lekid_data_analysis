import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import matplotlib.pyplot as plt
import skrf as rf
import os

directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\FBS_LEKIDs\hybrid_idc_lekid\feedline_data"
filename = "feedline_with_4_lekid_holes_unit_cell_cpw_onlys_param1.s2p"
plot_db = True
plot_power = False

unit_cell_repeats_array = [0, 10, 50, 200]

file_path = os.path.join(directory, filename)

# Read current touchstone file
touchstone_file = rf.Touchstone(file_path)

unit_network = util.sonnet_simulation_tools.make_renormalised_network(
    data_file_path=file_path, n_ports=2
)

frequency = unit_network.f

main_network = unit_network

plt.figure(figsize=(8, 6))
for unit_cell_repeats in unit_cell_repeats_array:
    for repeat in range(unit_cell_repeats - 1):
        main_network = main_network ** unit_network

    plt.plot(main_network.f * 1e-9, main_network.s_db[:, 0, 1], label=str(unit_cell_repeats*4))

plt.xlabel("Frequency (GHz)")
plt.ylabel("S21 (dB)")
plt.legend(title="Number of KIDs on line:")
plt.show()
