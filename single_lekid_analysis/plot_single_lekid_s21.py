import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import os

directory = r"..\\data"
filename = "hybrid_idc_lekid_cross_coupling_two_opposite_lekids_param03.s4p"
plot_db = True
plot_power = False

file_path = os.path.join(directory, filename)

# Read current touchstone file
touchstone_file = rf.Touchstone(file_path)
# Get touchstone variables from comments
variables = touchstone_file.get_comment_variables()
variables_string = ""
n = 1
for variable in variables:
    if variable == list(variables.keys())[-1]:
        variables_string += variable + "=" + str(variables[variable][0])
        continue
    variables_string += variable + "=" + str(variables[variable][0]) + ", "
    if len(variables_string) > 60 * n:
        n += 1
        variables_string += "\n"

network = util.sonnet_simulation_tools.make_renormalised_network(
    data_file_path=file_path, n_ports=4
)

frequency = network.f

util.sonnet_simulation_tools.plot_network_s_params(
    network=network,
    from_port=1,
    to_port=2,
    plot_title="LEKID with " + variables_string,
    plot_db=plot_db,
    plot_power=plot_power,
)
