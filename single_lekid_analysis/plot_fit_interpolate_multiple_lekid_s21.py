import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import interpolate
import os


def main():
    directory = r"..\data"
    plot_db = True
    show_all_plots = False
    desired_qc = 50000

    fit_dictionary = {
        "label": [],
        "idc_length": [],
        "coupler_length": [],
        "f0": [],
        "Qr": [],
        "Qc": [],
        "Qi": [],
    }

    ########################################################################################################################

    # Section to collect data from data files

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        if ".s2p" in filename:
            # Read current touchstone file
            touchstone_file = rf.Touchstone(file_path)
            # Get touchstone variables from comments
            variables = touchstone_file.get_comment_variables()
            variables_string = ""
            for variable in variables:
                if variable == list(variables.keys())[-1]:
                    variables_string += variable + "=" + str(variables[variable][0])
                    continue
                variables_string += variable + "=" + str(variables[variable][0]) + ", "

            idc_length = float(variables["idc_length"][0])
            coupler_length = float(variables["coupler_length"][0])

            network = util.sonnet_simulation_tools.make_renormalised_network(
                data_file_path=file_path, n_ports=2
            )

            frequency = network.f

            fit_params = util.lekid_analysis_tools.fit_skewed_lorentzian(
                frequency_array=frequency,
                data_array=network.s[:, 0, 1],
                qr_guess=1000.0,
                f0_guess=None,
                plot_graph=show_all_plots,
                plot_dB=plot_db,
                plot_title="LEKID with " + variables_string,
            )

            f0 = fit_params["f0"][0]
            qr = fit_params["qr"][0]
            qc = fit_params["qc"][0]
            qi = fit_params["qi"][0]

            fit_dictionary["label"].append(
                "IDC length=" + str(idc_length) + ", coupler=" + str(coupler_length)
            )
            fit_dictionary["idc_length"].append(idc_length)
            fit_dictionary["coupler_length"].append(coupler_length)
            fit_dictionary["f0"].append(f0)
            fit_dictionary["Qr"].append(qr)
            fit_dictionary["Qc"].append(qc)
            fit_dictionary["Qi"].append(qi)

    # Convert fit result arrays to numpy arrays:
    fit_dictionary["idc_length"] = np.array(fit_dictionary["idc_length"])
    fit_dictionary["coupler_length"] = np.array(fit_dictionary["coupler_length"])
    fit_dictionary["f0"] = np.array(fit_dictionary["f0"])
    fit_dictionary["Qr"] = np.array(fit_dictionary["Qr"])
    fit_dictionary["Qc"] = np.array(fit_dictionary["Qc"])
    fit_dictionary["Qi"] = np.array(fit_dictionary["Qi"])

    ########################################################################################################################

    # Plot fit summaries

    results_dataframe = pd.DataFrame(fit_dictionary)
    results_dataframe.plot.barh(x="label", y=["Qr", "Qc", "Qi"], logx=True)
    plt.tight_layout()
    plt.show()

    results_dataframe.plot.barh(x="label", y="f0")
    plt.tight_layout()
    plt.show()

    ########################################################################################################################

    # Section to interpolate the required coupler length for each idc_length to give desired_qc

    # Section to make new sub_arrays where each sub_array corresponds to one idc length:
    idc_length_sub_arrays = []
    coupler_length_sub_arrays = []
    qr_sub_arrays = []
    qc_sub_arrays = []
    qi_sub_arrays = []

    unique_idc_lengths = np.unique(fit_dictionary["idc_length"])
    for unique_length in unique_idc_lengths:
        current_length_indices = np.where(
            fit_dictionary["idc_length"] == unique_length
        )[0]
        # Add sub array of values corresponding to current length indices to relevant subarrays list
        idc_length_sub_arrays.append(
            fit_dictionary["idc_length"][current_length_indices]
        )
        coupler_length_sub_arrays.append(
            fit_dictionary["coupler_length"][current_length_indices]
        )
        qr_sub_arrays.append(fit_dictionary["Qr"][current_length_indices])
        qc_sub_arrays.append(fit_dictionary["Qc"][current_length_indices])
        qi_sub_arrays.append(fit_dictionary["Qi"][current_length_indices])

    idc_length_sub_arrays = np.array(idc_length_sub_arrays)
    coupler_length_sub_arrays = np.array(coupler_length_sub_arrays)
    qr_sub_arrays = np.array(qr_sub_arrays)
    qc_sub_arrays = np.array(qc_sub_arrays)
    qi_sub_arrays = np.array(qi_sub_arrays)

    required_coupler_lengths = []
    for count, qc_array in enumerate(qc_sub_arrays):
        coupler_length_interpolant = interpolate.interp1d(
            qc_array, coupler_length_sub_arrays[count]
        )  # get interpolating function
        required_coupler_lengths.append(
            float(coupler_length_interpolant(desired_qc))
        )  # Extract required coupler length from interpolant.

    # Convert to numpy arrays:
    required_coupler_lengths = np.array(required_coupler_lengths)

    # Fit polynomial:
    coefficients = np.polyfit(unique_idc_lengths, required_coupler_lengths, 2)
    coupler_polynomial = np.poly1d(coefficients)
    x_points = np.linspace(
        np.min(unique_idc_lengths),
        np.max(unique_idc_lengths),
        np.size(unique_idc_lengths) * 100,
    )
    y_fit = coupler_polynomial(x_points)

    plt.figure(figsize=(8, 6))
    plt.plot(
        unique_idc_lengths,
        required_coupler_lengths,
        linestyle="none",
        color="b",
        marker="o",
    )
    plt.plot(
        x_points,
        y_fit,
        linestyle="-",
        color="r",
        label="Fit with curve:\n" + str(coupler_polynomial),
    ),
    plt.xlabel("IDC Length (um)")
    plt.ylabel("Coupler Length (um)")
    plt.title(
        "Coupler lengths required to create a LEKID\nwith Qc=%0.1e for a given IDC length"
        % (desired_qc)
    )
    plt.legend()
    plt.show()

    ########################################################################################################################

    # Section to interpolate the required idc length for a given f0.

    # Create high sampled arrays of IDC length and the required coupler lengths to use for interpolating f0.
    idc_length_array = np.linspace(
        start=np.min(unique_idc_lengths), stop=np.max(unique_idc_lengths), num=100
    )
    required_coupler_length_array = coupler_polynomial(idc_length_array)

    f0_array = interpolate.griddata(
        points=(fit_dictionary["idc_length"], fit_dictionary["coupler_length"]),
        values=fit_dictionary["f0"],
        xi=(idc_length_array, required_coupler_length_array),
    )
    f0_array = np.array(f0_array)

    plt.figure(figsize=(8, 6))
    plt.plot(f0_array * 1e-9, idc_length_array, linestyle="none", color="b", marker="o")
    plt.xlabel("f0 (GHz)")
    plt.ylabel("IDC Length (um)")
    plt.title(
        "The resonant frequency of a LEKID with variable IDC lengths using the\ncoupler lengths required to give Qc=%0.1e"
        % (desired_qc)
    )
    plt.show()

    # Fit polynomial:
    coefficients = np.polyfit(f0_array, idc_length_array, 3)
    idc_length_polynomial = np.poly1d(coefficients)
    x_points = np.linspace(np.min(f0_array), np.max(f0_array), np.size(f0_array) * 100)
    y_fit = idc_length_polynomial(x_points)

    plt.figure(figsize=(8, 6))
    plt.plot(f0_array * 1e-9, idc_length_array, linestyle="none", color="b", marker="o")
    plt.plot(
        x_points * 1e-9,
        y_fit,
        linestyle="-",
        linewidth=4,
        color="r",
        label="Fit with curve:\n" + str(idc_length_polynomial),
    ),
    plt.xlabel("f0 (GHz)")
    plt.ylabel("IDC Length (um)")
    plt.title(
        "The resonant frequency of a LEKID with variable IDC lengths using the\ncoupler lengths required to give Qc=%0.1e"
        % (desired_qc)
    )
    plt.legend()
    plt.show()

    print(
        "The LEKID design polynomials for a given f0 are:\nIDC Length=\n"
        + str(idc_length_polynomial)
        + "\nWhere x = f0.\nCoupler Length=\n"
        + str(coupler_polynomial)
        + "\nWhere x=IDC_length"
    )


if __name__ == "__main__":
    main()
