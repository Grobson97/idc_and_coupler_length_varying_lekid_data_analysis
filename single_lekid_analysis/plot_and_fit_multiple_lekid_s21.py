import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os


def main():
    directory = r"data"
    plot_db = True
    show_all_plots = True
    use_filename_as_label = (
        True  # If false, idc_length and coupler_length will be used as label.
    )

    fit_dictionary = {
        "label": [],
        "idc_length": [],
        "coupler_length": [],
        "f0": [],
        "Qr": [],
        "Qc": [],
        "Qi": [],
    }

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        if ".s2p" in filename:
            model_label = filename.replace(".s2p", "")
            # Read current touchstone file
            touchstone_file = rf.Touchstone(file_path)
            # Get touchstone variables from comments
            variables = touchstone_file.get_comment_variables()
            variables_string = ""
            for variable in variables:
                if variable == list(variables.keys())[-1]:
                    variables_string += variable + "=" + str(variables[variable][0])
                    continue
                variables_string += variable + "=" + str(variables[variable][0]) + ", "

            idc_length = float(variables["idc_length"][0])
            coupler_length = 3
            # coupler_length = float(variables["coupler_length"][0])

            network = util.sonnet_simulation_tools.make_renormalised_network(
                data_file_path=file_path, n_ports=2
            )

            frequency = network.f

            plot_title = "LEKID with " + variables_string
            if use_filename_as_label:
                plot_title = model_label

            fit_params = util.lekid_analysis_tools.fit_skewed_lorentzian(
                frequency_array=frequency,
                data_array=network.s[:, 0, 1],
                qr_guess=1e6,
                f0_guess=None,
                plot_graph=show_all_plots,
                plot_dB=plot_db,
                plot_title=plot_title,
            )

            f0 = fit_params["f0"][0]
            qr = fit_params["qr"][0]
            qc = fit_params["qc"][0]
            qi = fit_params["qi"][0]

            if use_filename_as_label:
                fit_dictionary["label"].append(model_label)
            else:
                fit_dictionary["label"].append(
                    "IDC length=" + str(idc_length) + ", coupler=" + str(coupler_length)
                )
            fit_dictionary["idc_length"].append(idc_length)
            fit_dictionary["coupler_length"].append(coupler_length)
            fit_dictionary["f0"].append(f0)
            fit_dictionary["Qr"].append(qr)
            fit_dictionary["Qc"].append(qc)
            fit_dictionary["Qi"].append(qi)

    fit_dictionary["idc_length"] = np.array(fit_dictionary["idc_length"])
    fit_dictionary["coupler_length"] = np.array(fit_dictionary["coupler_length"])
    fit_dictionary["f0"] = np.array(fit_dictionary["f0"])
    fit_dictionary["Qr"] = np.array(fit_dictionary["Qr"])
    fit_dictionary["Qc"] = np.array(fit_dictionary["Qc"])
    fit_dictionary["Qi"] = np.array(fit_dictionary["Qi"])

    # Plot summary of Q's
    results_dataframe = pd.DataFrame(fit_dictionary)
    results_dataframe.plot.barh(x="label", y=["Qr", "Qc", "Qi"], logx=True)
    plt.tight_layout()
    plt.show()

    results_dataframe.plot.barh(x="label", y="f0")
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    main()
