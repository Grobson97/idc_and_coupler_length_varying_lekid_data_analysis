import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os


def main():
    directory = r"..\data"
    plot_db = True
    plot_power = False

    plt.figure(figsize=(8, 6))
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)
        if ".s2p" in filename:
            network = util.sonnet_simulation_tools.make_renormalised_network(
                data_file_path=file_path, n_ports=2
            )

            y_label = "S21"
            if plot_db:
                plt.plot(network.f * 1e-9, network.s_db[:, 0, 1])
                y_label = "S21 Magnitude (dB)"
            if not plot_db:
                plt.plot(network.f * 1e-9, network.s_db[:, 0, 1])
                y_label = "S21 Magnitude (AU)"
            if plot_power:
                plt.plot(network.f * 1e-9, np.abs(network.s[:, 0, 1] ** 2))
                y_label = "S21 Power (W)"

    plt.xlabel("Frequency (GHz)")
    plt.ylabel(y_label)
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    main()
