import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os


def main():
    variable_name = "length_to_idc"  # must match variable_value name used in sonnet.
    directory = r"data"
    plot_db = True
    show_all_plots = False
    use_filename_as_label = (
        True  # If false, idc_length and coupler_length will be used as label.
    )

    fit_dictionary = {
        variable_name: [],
        "f0": [],
        "Qr": [],
        "Qc": [],
        "Qi": [],
    }

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        if ".s2p" in filename:
            model_label = filename.replace(".s2p", "")
            # Read current touchstone file
            touchstone_file = rf.Touchstone(file_path)
            # Get touchstone variables from comments
            variables = touchstone_file.get_comment_variables()

            variable_value = float(variables[variable_name][0])

            network = util.sonnet_simulation_tools.make_renormalised_network(
                data_file_path=file_path, n_ports=2
            )
            frequency = network.f

            plot_title = "LEKID with varying " + variable_name

            fit_params = util.lekid_analysis_tools.fit_skewed_lorentzian(
                frequency_array=frequency,
                data_array=network.s[:, 0, 1],
                qr_guess=1400,
                f0_guess=None,
                plot_graph=show_all_plots,
                plot_dB=plot_db,
                plot_title=plot_title,
            )

            fit_dictionary[variable_name].append(variable_value)
            fit_dictionary["f0"].append(fit_params["f0"])
            fit_dictionary["Qr"].append(fit_params["qr"])
            fit_dictionary["Qc"].append(fit_params["qc"])
            fit_dictionary["Qi"].append(fit_params["qi"])

    fit_dictionary[variable_name] = np.array(fit_dictionary[variable_name])
    fit_dictionary["f0"] = np.array(fit_dictionary["f0"])
    fit_dictionary["Qr"] = np.array(fit_dictionary["Qr"])
    fit_dictionary["Qc"] = np.array(fit_dictionary["Qc"])
    fit_dictionary["Qi"] = np.array(fit_dictionary["Qi"])

    plt.figure(figsize=(8, 6))
    plt.errorbar(
        fit_dictionary[variable_name],
        fit_dictionary["Qr"][:, 0],
        yerr=fit_dictionary["Qr"][:, 1],
        label="Qr",
        linestyle="none",
        marker="o",
    )
    plt.errorbar(
        fit_dictionary[variable_name],
        fit_dictionary["Qc"][:, 0],
        yerr=fit_dictionary["Qc"][:, 1],
        label="Qc",
        linestyle="none",
        marker="o",
    )
    plt.title("Q's of " + plot_title)
    plt.xlabel(variable_name)
    plt.ylabel("Quality Factor")
    plt.legend()
    plt.show()

    plt.figure(figsize=(8, 6))
    plt.errorbar(
        fit_dictionary[variable_name],
        fit_dictionary["f0"][:, 0] * 1e-9,
        yerr=fit_dictionary["f0"][:, 1] * 1e-9,
        linestyle="none",
        marker="o",
    )
    plt.title("f0's of " + plot_title)
    plt.xlabel(variable_name)
    plt.ylabel("f0 (GHz)")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
