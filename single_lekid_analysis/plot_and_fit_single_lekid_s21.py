import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os

directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\FBS_LEKIDs\hybrid_idc_lekid\cross_coupling_data\two_series_lekids\broken_inductor\f01_2p035GHz"
filename = "hybrid_idc_lekid_cross_coupling_two_lekids_broken_inductor_single_lekid_big box_param1.s2p"
plot_db = True


file_path = os.path.join(directory, filename)

# Read current touchstone file
touchstone_file = rf.Touchstone(file_path)
# Get touchstone variables from comments
variables = touchstone_file.get_comment_variables()
variables_string = ""
for variable in variables:
    if variable == list(variables.keys())[-1]:
        variables_string += variable + "=" + str(variables[variable][0])
        continue
    variables_string += variable + "=" + str(variables[variable][0]) + ", "

network = util.sonnet_simulation_tools.make_renormalised_network(
    data_file_path=file_path, n_ports=2
)

frequency = network.f

fit_params = util.lekid_analysis_tools.fit_skewed_lorentzian(
    frequency_array=frequency,
    data_array=network.s[:, 0, 1],
    qr_guess=5e4,
    f0_guess=None,
    plot_graph=True,
    plot_dB=plot_db,
    plot_title="LEKID with " + variables_string,
)

print(
    "Lekid characteristics:\nf0 = %0.2e\nQr = %0.2e\nQc = %0.2e\nQi = %0.2e"
    % (
        fit_params["f0"][0],
        fit_params["qr"][0],
        fit_params["qc"][0],
        fit_params["qi"][0],
    )
)
