import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import scipy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os


def lekid_idc_length(target_f0):
    """
    Function to calculate the length of the IDC for a hybrid LEKID at a given f0.

    :param target_f0: F0 of LEKID resonator.
    :return:
    """

    return (
        -2.558e-25 * target_f0**3
        + 2.237e-15 * target_f0**2
        - 7.461e-06 * target_f0
        + 9096
    )


def lekid_coupler_length(idc_length):
    """
    Function to calculate the length of the coupler for a hybrid sparse_lekid at a given IDC length.

    :param idc_length: Length of IDC.
    :return:
    """
    return -1.083e-5 * idc_length**2 + 0.06206 * idc_length + 93.47


def calculate_linewidth_shifts(full_data_dict: dict, data_group_indices: list, data_group_dark_index: int):
    """
    Function to calculate the line width shift in f02 and the fractional frequency shift of f01 for each separation data
    group.

    :param full_data_dict: Full data dictionary.
    :param data_group_indices: Indices corresponding to each separation group.
    :param data_group_dark_index: Index corresponding to data with the "dark" data. (Lk = 0.6)
    :return:
    """
    group_line_width_shifts = []
    group_k = [] # Array of fractional frequency shifts in f01 relative to f02 shifts
    for count, index in enumerate(data_group_indices):
        if index != data_group_dark_index:
            f0_1 = full_data_dict["data"][index]["f0_1"]
            dark_f0_1 = full_data_dict["data"][data_group_dark_index]["f0_1"]
            f0_2 = full_data_dict["data"][index]["f0_2"]
            dark_f0_2 = full_data_dict["data"][data_group_dark_index]["f0_2"]
            group_line_width_shifts.append(50000 * (f0_2 - dark_f0_2) / dark_f0_2)
            group_k.append((f0_1 - dark_f0_1) / (f0_2 - dark_f0_2))

    return np.array(group_line_width_shifts), np.array(group_k)


def main():
    directory = r"..\data"
    plot_db = True
    show_all_plots = False
    use_filename_as_label = (
        True  # If false, idc_length and coupler_length will be used as label.
    )

    full_data_dictionary = {
        "separation": [],
        "lk": [],
        "data": []
    }

    num_files = 0
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if ".s2p" in filename:
            num_files += 1

    figure, axis = plt.subplots(num_files + 1, 2, sharex="col", sharey="all")
    for file_index, file in enumerate(os.listdir(directory)):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        fit_dictionary = {}

        if ".s2p" in filename:
            model_label = filename.replace(".s2p", "")
            # Read current touchstone file
            touchstone_file = rf.Touchstone(file_path)
            # Get touchstone variables from comments
            variables = touchstone_file.get_comment_variables()
            target_f0_1 = float(variables["f0_1"][0])
            target_f0_2 = float(variables["f0_2"][0])
            separation = float(variables["separation"][0])
            lk_1 = 0.6
            lk_2 = float(variables["al_Lk"][0])

            network = util.sonnet_simulation_tools.make_renormalised_network(
                data_file_path=file_path, n_ports=2
            )
            frequency = network.f

            peak_indices = scipy.signal.find_peaks(
                abs(network.s_db[:, 0, 1]), prominence=1, height=1
            )[0]

            if len(peak_indices) > 1:
                mid_peaks_index = int((peak_indices[0] + peak_indices[1])/2)

                # Section to ensure correct range for fitting of each peak is sampled
                range_1 = [peak_indices[0] - 300, peak_indices[0] + 300]
                if range_1[0] < 0:
                    range_1[0] = 0
                if range_1[1] > mid_peaks_index:
                    range_1[1] = mid_peaks_index

                range_2 = [peak_indices[1] - 300, peak_indices[1] + 300]
                if range_2[0] < mid_peaks_index:
                    range_2[0] = mid_peaks_index
                if range_2[1] > len(frequency):
                    range_2[1] = -1

                # Fit to peak 1:
                fit_params_peak_1 = util.lekid_analysis_tools.fit_skewed_lorentzian(
                    frequency_array=frequency[range_1[0]:range_1[1]],
                    data_array=network.s[:, 0, 1][range_1[0]:range_1[1]],
                    qr_guess=1e5,
                    f0_guess=None,
                    plot_graph=show_all_plots,
                    plot_dB=plot_db,
                    plot_title="Peak 1 fit",
                )

                # Fit to peak 2:
                fit_params_peak_2 = util.lekid_analysis_tools.fit_skewed_lorentzian(
                    frequency_array=frequency[range_2[0]:range_2[1]],
                    data_array=network.s[:, 0, 1][range_2[0]:range_2[1]],
                    qr_guess=1e5,
                    f0_guess=None,
                    plot_graph=show_all_plots,
                    plot_dB=plot_db,
                    plot_title="Peak 2 fit",
                )

                axis[file_index, 0].plot(
                    frequency[0:mid_peaks_index],
                    network.s[:, 0, 1][0:mid_peaks_index],
                    color="b",
                )
                axis[file_index, 0].plot(
                    fit_params_peak_1["fit_f_array"],
                    fit_params_peak_1["fit_array"],
                    color="r",
                )
                axis[file_index, 0].set_title(
                    "Target f0_2 = %0.2e: Peak 1 fit" % target_f0_1
                )
                axis[file_index, 1].plot(
                    frequency[mid_peaks_index:-1],
                    network.s[:, 0, 1][mid_peaks_index:-1],
                    color="b",
                )
                axis[file_index, 1].plot(
                    fit_params_peak_2["fit_f_array"],
                    fit_params_peak_2["fit_array"],
                    color="r",
                )
                axis[file_index, 1].set_title(
                    "Target f0_2 = %0.2e: Peak 2 fit" % target_f0_1
                )

                # Logic to for lekids swapping places in frequency space. e.g. lekid_2 becomes higher f resonance
                lekid_1_fit_params = fit_params_peak_2
                lekid_2_fit_params = fit_params_peak_1
                if target_f0_2 > target_f0_1:
                    lekid_1_fit_params = fit_params_peak_1
                    lekid_2_fit_params = fit_params_peak_2

                fit_dictionary["s21"] = network.s
                fit_dictionary["frequency"] = network.f
                fit_dictionary["target_f0_1"] = target_f0_1
                fit_dictionary["f0_1"] = lekid_1_fit_params["f0"][0]
                fit_dictionary["Qr_1"] = lekid_1_fit_params["qr"][0]
                fit_dictionary["Qc_1"] = lekid_1_fit_params["qc"][0]
                fit_dictionary["Qi_1"] = lekid_1_fit_params["qi"][0]

                fit_dictionary["target_f0_2"] = target_f0_2
                fit_dictionary["f0_2"] = lekid_2_fit_params["f0"][0]
                fit_dictionary["Qr_2"] = lekid_2_fit_params["qr"][0]
                fit_dictionary["Qc_2"] = lekid_2_fit_params["qc"][0]
                fit_dictionary["Qi_2"] = lekid_2_fit_params["qi"][0]

                full_data_dictionary["separation"].append(separation)
                full_data_dictionary["lk"].append(lk_2)
                full_data_dictionary["data"].append(fit_dictionary)

    plt.tight_layout()
    plt.show()

########################################################################################################################

    # Plotting each groups S21 vs frequency.

    full_data_dictionary["separation"] = np.array(full_data_dictionary["separation"])
    full_data_dictionary["lk"] = np.array(full_data_dictionary["lk"])

    group_1_indices = np.where(full_data_dictionary["separation"] == 258.0)[0]
    group_2_indices = np.where(full_data_dictionary["separation"] == 387.0)[0]
    group_3_indices = np.where(full_data_dictionary["separation"] == 516.0)[0]
    group_4_indices = np.where(full_data_dictionary["separation"] == 645.0)[0]
    group_5_indices = np.where(full_data_dictionary["separation"] == 774.0)[0]

    plt.figure(figsize=(8, 6))
    for count, index in enumerate(group_1_indices):
        plt.plot(
            full_data_dictionary["data"][index]["frequency"] * 1e-9,
            20 * np.log10(np.abs(full_data_dictionary["data"][index]["s21"][:, 0, 1])),
            label=str(full_data_dictionary["lk"][index]),
        )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 (dB)")
    plt.legend(title="Lk_2:")
    plt.title("S21 data for 258um separation simulations")
    plt.show()

    plt.figure(figsize=(8, 6))
    for count, index in enumerate(group_2_indices):
        plt.plot(
            full_data_dictionary["data"][index]["frequency"] * 1e-9,
            20 * np.log10(np.abs(full_data_dictionary["data"][index]["s21"][:, 0, 1])),
            label=str(full_data_dictionary["lk"][index]),
        )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 (dB)")
    plt.legend(title="Lk_2:")
    plt.title("S21 data for 387um separation simulations")
    plt.show()

    plt.figure(figsize=(8, 6))
    for count, index in enumerate(group_3_indices):
        plt.plot(
            full_data_dictionary["data"][index]["frequency"] * 1e-9,
            20 * np.log10(np.abs(full_data_dictionary["data"][index]["s21"][:, 0, 1])),
            label=str(full_data_dictionary["lk"][index]),
        )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 (dB)")
    plt.legend(title="Lk_2:")
    plt.title("S21 data for 516um separation simulations")
    plt.show()

    plt.figure(figsize=(8, 6))
    for count, index in enumerate(group_4_indices):
        plt.plot(
            full_data_dictionary["data"][index]["frequency"] * 1e-9,
            20 * np.log10(np.abs(full_data_dictionary["data"][index]["s21"][:, 0, 1])),
            label=str(full_data_dictionary["lk"][index]),
        )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 (dB)")
    plt.legend(title="Lk_2:")
    plt.title("S21 data for 645um separation simulations")
    plt.show()

    plt.figure(figsize=(8, 6))
    for count, index in enumerate(group_5_indices):
        plt.plot(
            full_data_dictionary["data"][index]["frequency"] * 1e-9,
            20 * np.log10(np.abs(full_data_dictionary["data"][index]["s21"][:, 0, 1])),
            label=str(full_data_dictionary["lk"][index]),
        )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 (dB)")
    plt.legend(title="Lk_2:")
    plt.title("S21 data for 774um separation simulations")
    plt.show()

########################################################################################################################

    # Calculating frequency shifts.

    group_1_dark_index = group_1_indices[np.where(full_data_dictionary["lk"][group_1_indices] == 0.6)[0]][0]
    group_2_dark_index = group_2_indices[np.where(full_data_dictionary["lk"][group_2_indices] == 0.6)[0]][0]
    group_3_dark_index = group_3_indices[np.where(full_data_dictionary["lk"][group_3_indices] == 0.6)[0]][0]
    group_4_dark_index = group_4_indices[np.where(full_data_dictionary["lk"][group_4_indices] == 0.6)[0]][0]
    group_5_dark_index = group_5_indices[np.where(full_data_dictionary["lk"][group_5_indices] == 0.6)[0]][0]

    group_1_line_width_shifts, group_1_k = calculate_linewidth_shifts(
        full_data_dict=full_data_dictionary,
        data_group_indices=group_1_indices,
        data_group_dark_index=group_1_dark_index
    )
    group_2_line_width_shifts, group_2_k = calculate_linewidth_shifts(
        full_data_dict=full_data_dictionary,
        data_group_indices=group_2_indices,
        data_group_dark_index=group_2_dark_index
    )
    group_3_line_width_shifts, group_3_k = calculate_linewidth_shifts(
        full_data_dict=full_data_dictionary,
        data_group_indices=group_3_indices,
        data_group_dark_index=group_3_dark_index
    )
    group_4_line_width_shifts, group_4_k = calculate_linewidth_shifts(
        full_data_dict=full_data_dictionary,
        data_group_indices=group_4_indices,
        data_group_dark_index=group_4_dark_index
    )
    group_5_line_width_shifts, group_5_k = calculate_linewidth_shifts(
        full_data_dict=full_data_dictionary,
        data_group_indices=group_5_indices,
        data_group_dark_index=group_5_dark_index
    )

########################################################################################################################

    unique_separations = np.array([258.0, 387.0, 516.0, 645.0, 774.0])
    percentage_df_at_10_line_widths = []

    def plot_percentage_df(group_line_width_shifts, group_k, separation):
        result = util.sonnet_simulation_tools.fit_linear_regression(
            group_line_width_shifts,
            y_data=group_k,
            plot_graph=True,
            plot_title="Fractional frequency shift for simulations with %.1f um separation" % separation,
            x_label="$\Delta f0_2$ (linewidths)",
            y_label="$\Delta f0_1/\Delta f0_2$",
        )
        line_widths = -10
        percentage_df = 100 * (result.slope * line_widths + result.intercept)
        if result.slope > 0:
            percentage_df = np.mean(group_k) * 100
        print("Percentage coupled frequency shift at %i linewidths shift = %.3f" % (line_widths, percentage_df))

        return percentage_df

    percentage_df_at_10_line_widths.append(plot_percentage_df(group_1_line_width_shifts, group_1_k, separation=258))
    percentage_df_at_10_line_widths.append(plot_percentage_df(group_2_line_width_shifts, group_2_k, separation=387))
    percentage_df_at_10_line_widths.append(plot_percentage_df(group_3_line_width_shifts, group_3_k, separation=516))
    percentage_df_at_10_line_widths.append(plot_percentage_df(group_4_line_width_shifts, group_4_k, separation=645))
    percentage_df_at_10_line_widths.append(plot_percentage_df(group_5_line_width_shifts, group_5_k, separation=774))

    plt.figure(figsize=(8, 6))
    plt.plot(unique_separations/(unique_separations[0]*4), percentage_df_at_10_line_widths, linestyle="none", marker="o")
    plt.xlabel("LEKID separation ($\lambda)")
    plt.ylabel("Frequency shift (%)")
    plt.title("Percentage coupled frequency shift of f01 at 10 line widths shift\nin f02 for various lekid separations with respect to the\nsmallest mm wavelength")
    plt.show()


if __name__ == "__main__":
    main()
