import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import scipy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os


def lekid_idc_length(target_f0):
    """
    Function to calculate the length of the IDC for a hybrid LEKID at a given f0.

    :param target_f0: F0 of LEKID resonator.
    :return:
    """

    return (
        -2.558e-25 * target_f0**3
        + 2.237e-15 * target_f0**2
        - 7.461e-06 * target_f0
        + 9096
    )


def lekid_idc_length_flipped(target_f0):
    """
    Function to calculate the length of the IDC for a hybrid LEKID at a given f0.

    :param target_f0: F0 of LEKID resonator.
    :return:
    """

    return (
        -2.914e-25 * target_f0**3
        + 2.556e-15 * target_f0**2
        - 8.472e-6 * target_f0
        + 1.023e4
    )


def lekid_coupler_length(idc_length):
    """
    Function to calculate the length of the coupler for a hybrid sparse_lekid at a given IDC length.

    :param idc_length: Length of IDC.
    :return:
    """
    return -1.083e-5 * idc_length**2 + 0.06206 * idc_length + 93.47


def lekid_coupler_length_flipped(idc_length):
    """
    Function to calculate the length of the coupler for a hybrid sparse_lekid at a given IDC length.

    :param idc_length: Length of IDC.
    :return:
    """
    return 1.833e-5 * idc_length**2 + 0.01281 * idc_length + 109


def main():
    directory = r"..\data"
    plot_db = True
    show_all_plots = False
    use_filename_as_label = (
        True  # If false, idc_length and coupler_length will be used as label.
    )

    fit_dictionary = {
        "s21": [],
        "frequency": [],
        "target_f0_1": [],
        "target_f0_2": [],
        "idc_length_1": [],
        "idc_length_2": [],
        "f0_1": [],
        "f0_2": [],
        "Qr_1": [],
        "Qr_2": [],
        "Qc_1": [],
        "Qc_2": [],
        "Qi_1": [],
        "Qi_2": [],
    }

    num_files = 0
    for file in os.listdir(directory):
        filename = os.fsdecode(file)

        if ".s2p" in filename:
            num_files += 1

    figure, axis = plt.subplots(num_files + 1, 2, sharex="all", sharey="all")
    for file_index, file in enumerate(os.listdir(directory)):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        if ".s2p" in filename:
            model_label = filename.replace(".s2p", "")
            # Read current touchstone file
            touchstone_file = rf.Touchstone(file_path)
            # Get touchstone variables from comments
            variables = touchstone_file.get_comment_variables()
            target_f0_1 = float(variables["f0_1"][0])
            target_f0_2 = float(variables["f0_2"][0])

            plot_title = (
                "Cross coupling with control lekid at f0 =  "
                + str(target_f0_1)
                + " GHz and variable f0 = "
                + str(target_f0_2)
                + " GHz"
            )

            network = util.sonnet_simulation_tools.make_renormalised_network(
                data_file_path=file_path, n_ports=2
            )
            frequency = network.f

            peak_indices = scipy.signal.find_peaks(
                abs(network.s_db[:, 0, 1]), height=5
            )[0]

            if len(peak_indices) == 2:
                mid_peaks_index = int(np.mean(peak_indices))

                # Fit to peak 1:
                fit_params_peak_1 = util.lekid_analysis_tools.fit_skewed_lorentzian(
                    frequency_array=frequency[0:mid_peaks_index],
                    data_array=network.s[:, 0, 1][0:mid_peaks_index],
                    qr_guess=1e6,
                    f0_guess=None,
                    plot_graph=show_all_plots,
                    plot_dB=plot_db,
                    plot_title="Peak 1 fit",
                )

                # Fit to peak 2:
                fit_params_peak_2 = util.lekid_analysis_tools.fit_skewed_lorentzian(
                    frequency_array=frequency[mid_peaks_index:-1],
                    data_array=network.s[:, 0, 1][mid_peaks_index:-1],
                    qr_guess=1e6,
                    f0_guess=None,
                    plot_graph=show_all_plots,
                    plot_dB=plot_db,
                    plot_title="Peak 2 fit",
                )

                axis[file_index, 0].plot(
                    frequency[0:mid_peaks_index],
                    network.s[:, 0, 1][0:mid_peaks_index],
                    color="b",
                )
                axis[file_index, 0].plot(
                    fit_params_peak_1["fit_f_array"],
                    fit_params_peak_1["fit_array"],
                    color="r",
                )
                axis[file_index, 0].set_title(
                    "Target f0_2 = %0.2e: Peak 1 fit" % target_f0_1
                )
                axis[file_index, 1].plot(
                    frequency[mid_peaks_index:-1],
                    network.s[:, 0, 1][mid_peaks_index:-1],
                    color="b",
                )
                axis[file_index, 1].plot(
                    fit_params_peak_2["fit_f_array"],
                    fit_params_peak_2["fit_array"],
                    color="r",
                )
                axis[file_index, 1].set_title(
                    "Target f0_2 = %0.2e: Peak 2 fit" % target_f0_1
                )

                # Logic to for lekids swapping places in frequency space. e.g. lekid_2 becomes higher f resonance
                lekid_1_fit_params = fit_params_peak_2
                lekid_2_fit_params = fit_params_peak_1
                if target_f0_2 > target_f0_1:
                    lekid_1_fit_params = fit_params_peak_1
                    lekid_2_fit_params = fit_params_peak_2

                fit_dictionary["s21"].append(network.s)
                fit_dictionary["frequency"].append(network.f)
                fit_dictionary["target_f0_1"].append(target_f0_1)
                fit_dictionary["idc_length_1"].append(
                    lekid_idc_length(target_f0_1 * 1e9)
                )
                fit_dictionary["f0_1"].append(lekid_1_fit_params["f0"][0])
                fit_dictionary["Qr_1"].append(lekid_1_fit_params["qr"][0])
                fit_dictionary["Qc_1"].append(lekid_1_fit_params["qc"][0])
                fit_dictionary["Qi_1"].append(lekid_1_fit_params["qi"][0])

                fit_dictionary["target_f0_2"].append(target_f0_2)
                fit_dictionary["idc_length_2"].append(
                    lekid_idc_length_flipped(target_f0_2 * 1e9)
                )
                fit_dictionary["f0_2"].append(lekid_2_fit_params["f0"][0])
                fit_dictionary["Qr_2"].append(lekid_2_fit_params["qr"][0])
                fit_dictionary["Qc_2"].append(lekid_2_fit_params["qc"][0])
                fit_dictionary["Qi_2"].append(lekid_2_fit_params["qi"][0])

    plt.tight_layout()
    plt.show()

    ########################################################################################################################

    plt.figure(figsize=(8, 6))
    for count, s21 in enumerate(fit_dictionary["s21"]):
        plt.plot(
            fit_dictionary["frequency"][count] * 1e-9,
            20 * np.log10(np.abs(s21[:, 0, 1])),
            label=str(fit_dictionary["target_f0_2"][count]),
        )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 (dB)")
    plt.legend(title="Target f0_2:")
    plt.title("Each simulations S21 data")
    plt.show()

    ########################################################################################################################

    # Plotting f0 of lekid 1 and 2 (coupled and non coupled) vs IDC length difference.

    for key in fit_dictionary:
        fit_dictionary[key] = np.array(fit_dictionary[key])

    plt.figure(figsize=(8, 6))
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["f0_1"] * 1e-9,
        linestyle="none",
        marker="o",
        label="f0_1",
    )
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["target_f0_1"],
        linestyle="--",
        color="k",
        markersize=6,
        linewidth=1,
        marker="x",
        label="Non-coupled f0_1",
    )
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["f0_2"] * 1e-9,
        linestyle="none",
        marker="o",
        label="f0_2",
    )
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["target_f0_2"],
        linestyle="dotted",
        color="k",
        markersize=8,
        linewidth=1,
        marker="+",
        label="Non-coupled f0_2",
    )
    plt.xlabel("IDC length difference (um)")
    plt.ylabel("f0 (GHz)")
    plt.legend()
    plt.show()

    ########################################################################################################################

    # Plotting difference between coupled and non-coupled f0 vs IDC length difference.

    plt.figure(figsize=(8, 6))
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["f0_1"] * 1e-9 - fit_dictionary["target_f0_1"],
        linestyle="none",
        marker="o",
        label="f0_1",
    )
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["f0_2"] * 1e-9 - fit_dictionary["target_f0_2"],
        linestyle="none",
        marker="o",
        label="f0_2",
    )
    plt.xlabel("Difference in IDC length (um)")
    plt.ylabel("coupled/non-coupled f0 difference (GHz)")
    plt.legend()
    plt.show()

    ########################################################################################################################

    # Plotting difference between f0_1 and f0_2 vs IDC length difference.

    # Fitting polynomial to df_split vs dl, finding minimum point.
    polynomial_fit, minima = util.lekid_analysis_tools.fit_polynomial_find_minima(
        x_data=fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        y_data=np.abs(fit_dictionary["f0_1"] * 1e-9 - fit_dictionary["f0_2"] * 1e-9),
        degree=3,
        plot_graph=True,
    )
    df_split = polynomial_fit(minima)

    plt.figure(figsize=(8, 6))
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        np.abs(fit_dictionary["f0_1"] * 1e-9 - fit_dictionary["f0_2"] * 1e-9),
        linestyle="none",
        marker="o",
    )

    plt.xlabel("Difference in IDC length (um)")
    plt.ylabel("|f0_1 - f0_2| (GHz)")
    plt.title("Minimum point (d$f_{split}) = %0.2e$ Hz" % (df_split * 1e9))
    plt.legend()
    plt.show()

    ####################################################################################################################

    plt.figure(figsize=(8, 6))
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["Qr_1"] * 1e-6,
        linestyle="none",
        marker="o",
        label="Qr_1",
    )
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["Qc_1"] * 1e-6,
        linestyle="none",
        marker="s",
        label="Qc_1",
    )
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["Qr_2"] * 1e-6,
        linestyle="none",
        marker="o",
        label="Qr_2",
    )
    plt.plot(
        fit_dictionary["idc_length_1"] - fit_dictionary["idc_length_2"],
        fit_dictionary["Qc_2"] * 1e-6,
        linestyle="none",
        marker="s",
        label="Qc_2",
    )
    plt.xlabel("Difference in IDC length (um)")
    plt.ylabel("Q * 1e6")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
