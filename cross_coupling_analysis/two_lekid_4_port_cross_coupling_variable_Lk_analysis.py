import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import scipy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os


def main():
    directory = r"..\data"
    plot_db = True
    show_all_plots = False
    use_filename_as_label = (
        True  # If false, idc_length and coupler_length will be used as label.
    )

    fit_dictionary = {
        "s_params": [],
        "frequency": [],
        "target_f0_1": [],
        "target_f0_2": [],
        "lk_1": [],
        "lk_2": [],
        "f0_1": [],
        "f0_2": [],
        "Qr_1": [],
        "Qr_2": [],
        "Qc_1": [],
        "Qc_2": [],
        "Qi_1": [],
        "Qi_2": [],
    }

    num_files = 0
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if ".s4p" in filename:
            num_files += 1

    figure, axis = plt.subplots(num_files + 1, 2, sharex="all", sharey="all")
    for file_index, file in enumerate(os.listdir(directory)):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        if ".s4p" in filename:
            model_label = filename.replace(".s2p", "")
            # Read current touchstone file
            touchstone_file = rf.Touchstone(file_path)
            # Get touchstone variables from comments
            variables = touchstone_file.get_comment_variables()
            target_f0_1 = float(variables["f0_1"][0])
            target_f0_2 = float(variables["f0_2"][0])
            lk_1 = 0.6
            lk_2 = float(variables["al_Lk"][0])

            network = util.sonnet_simulation_tools.make_renormalised_network(
                data_file_path=file_path, n_ports=4
            )
            frequency = network.f

            # Fit to peak 1:
            fit_params_peak_1 = util.lekid_analysis_tools.fit_skewed_lorentzian(
                frequency_array=frequency,
                data_array=network.s[:, 0, 1],
                qr_guess=1e4,
                f0_guess=None,
                plot_graph=show_all_plots,
                plot_dB=plot_db,
                plot_title="Peak 1 fit",
            )

            # Fit to peak 2:
            fit_params_peak_2 = util.lekid_analysis_tools.fit_skewed_lorentzian(
                frequency_array=frequency,
                data_array=network.s[:, 2, 3],
                qr_guess=1e4,
                f0_guess=None,
                plot_graph=show_all_plots,
                plot_dB=plot_db,
                plot_title="Peak 2 fit",
            )

            axis[file_index, 0].plot(
                frequency,
                network.s[:, 0, 1],
                color="b",
            )
            axis[file_index, 0].plot(
                fit_params_peak_1["fit_f_array"],
                fit_params_peak_1["fit_array"],
                color="r",
            )
            axis[file_index, 0].set_title(
                "Target f0_2 = %0.2e: Peak 1 fit" % target_f0_1
            )
            axis[file_index, 1].plot(
                frequency,
                network.s[:, 2, 3],
                color="b",
            )
            axis[file_index, 1].plot(
                fit_params_peak_2["fit_f_array"],
                fit_params_peak_2["fit_array"],
                color="r",
            )
            axis[file_index, 1].set_title(
                "Target f0_2 = %0.2e: Peak 2 fit" % target_f0_1
            )

            # Logic to for lekids swapping places in frequency space. e.g. lekid_2 becomes higher f resonance
            lekid_1_fit_params = fit_params_peak_2
            lekid_2_fit_params = fit_params_peak_1
            if target_f0_2 > target_f0_1:
                lekid_1_fit_params = fit_params_peak_1
                lekid_2_fit_params = fit_params_peak_2

            fit_dictionary["s_params"].append(network.s)
            fit_dictionary["frequency"].append(network.f)
            fit_dictionary["target_f0_1"].append(target_f0_1)
            fit_dictionary["lk_1"].append(lk_1)
            fit_dictionary["f0_1"].append(lekid_1_fit_params["f0"][0])
            fit_dictionary["Qr_1"].append(lekid_1_fit_params["qr"][0])
            fit_dictionary["Qc_1"].append(lekid_1_fit_params["qc"][0])
            fit_dictionary["Qi_1"].append(lekid_1_fit_params["qi"][0])

            fit_dictionary["target_f0_2"].append(target_f0_2)
            fit_dictionary["lk_2"].append(lk_2)
            fit_dictionary["f0_2"].append(lekid_2_fit_params["f0"][0])
            fit_dictionary["Qr_2"].append(lekid_2_fit_params["qr"][0])
            fit_dictionary["Qc_2"].append(lekid_2_fit_params["qc"][0])
            fit_dictionary["Qi_2"].append(lekid_2_fit_params["qi"][0])

    plt.tight_layout()
    plt.show()

    print(
        "Dark f0 of LEKID 1 = %.3e Hz\nDark f0 of LEKID 2 = %.3e Hz\nActual df_split = %.3e Hz"
        % (
            fit_dictionary["f0_1"][0],
            fit_dictionary["f0_2"][0],
            fit_dictionary["f0_2"][0] - fit_dictionary["f0_1"][0],
        )
    )

    ########################################################################################################################

    plt.figure(figsize=(8, 6))
    for count, s_params in enumerate(fit_dictionary["s_params"]):
        plt.plot(
            fit_dictionary["frequency"][count] * 1e-9,
            20 * np.log10(np.abs(s_params[:, 0, 1])),
            label=str(fit_dictionary["lk_2"][count]) + " S21",
        )
        plt.plot(
            fit_dictionary["frequency"][count] * 1e-9,
            20 * np.log10(np.abs(s_params[:, 2, 3])),
            linestyle=":",
            label=str(fit_dictionary["lk_2"][count]) + " S43",
        )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S Param (dB)")
    plt.legend(title="Lk_2:")
    plt.title("Each simulations S21 and S43 data")
    plt.show()

    ########################################################################################################################

    for key in fit_dictionary:
        fit_dictionary[key] = np.array(fit_dictionary[key])

    # Plotting f0 of lekid 1 as a function of the change in inductance of lekid 2

    figure, axes = plt.subplots(2, 1, sharex=True, figsize=(8, 6))
    figure.suptitle(
        "Plotting f0 and df0/d$\Delta$f0_2 of lekid 1 as a function of the shift in f0_2"
    )
    axes[0].plot(
        (fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6,
        (fit_dictionary["f0_1"] * 1e-9),
        linestyle="none",
        marker="o",
        label="Coupled f0",
    )
    axes[0].hlines(
        fit_dictionary["target_f0_1"],
        xmin=min((fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6),
        xmax=max((fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6),
        linestyles="--",
        color="k",
        label="Non coupled f0",
    )
    axes[0].legend()
    axes[1].plot(
        (fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6,
        np.gradient(fit_dictionary["f0_1"] * 1e-9),
        linestyle="none",
        marker="o",
        label="f0_1",
    )
    axes[0].set_ylabel("f0 (GHz)")
    plt.xlabel(
        "$\Delta f0_2$ (MHz)",
    )
    axes[1].set_ylabel("df0/d$\Delta$f0_2")
    plt.tight_layout()
    plt.show()

    for key in fit_dictionary:
        fit_dictionary[key] = np.array(fit_dictionary[key])

    # Plotting f0 of lekid 1 as a function of the change in inductance of lekid 2

    figure, axes = plt.subplots(2, 1, sharex=True, figsize=(8, 6))
    figure.suptitle(
        "Plotting f0 and df0/d$\Delta$f0_2 of lekid 2 as a function of the shift in f0_2"
    )
    axes[0].plot(
        (fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6,
        (fit_dictionary["f0_2"] * 1e-9),
        linestyle="none",
        marker="o",
        label="Coupled f0",
    )
    axes[0].hlines(
        fit_dictionary["target_f0_2"],
        xmin=min(
            (fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6,
        ),
        xmax=max(
            (fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6,
        ),
        linestyles="--",
        color="k",
        label="Non coupled f0",
    )
    axes[0].legend()
    axes[1].plot(
        (fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6,
        np.gradient(fit_dictionary["f0_2"] * 1e-9),
        linestyle="none",
        marker="o",
        label="f0_1",
    )
    axes[0].set_ylabel("f0 (GHz)")
    plt.xlabel(
        "$\Delta f0_2$ (MHz)",
    )
    axes[1].set_ylabel("df0/$\Delta$f0_2")
    plt.tight_layout()
    plt.show()

    ########################################################################################################################

    # Plotting f0 of lekid 1 and 2 (coupled and non coupled) vs IDC length difference.
    k = (fit_dictionary["f0_1"] - fit_dictionary["f0_1"][0]) / (
        fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]
    )

    figure, axes = plt.subplots(2, 1, sharex=True, figsize=(8, 6))
    figure.suptitle(
        "Plotting the ratio of the change in f0_1 due to a change in f0_2 caused by a\nshift in f0_2."
    )
    axes[0].plot(
        (fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6,
        k,
        linestyle="--",
        marker="o",
        label="f0_1",
    )
    axes[0].legend()
    axes[1].plot(
        (fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6,
        np.gradient(k),
        linestyle="--",
        marker="o",
        label="f0_1",
    )
    axes[0].set_ylabel("k = |$f0_1 - f0_{1, dark}$| / |$f0_2 - f0_{2, dark}$|")
    axes[1].set_xlabel("|$f0_2 - f0_{2, dark}$| (MHz)")
    axes[1].set_ylabel("dk/dL (sq pH$^{-1}$)")
    plt.tight_layout()
    plt.show()

    util.sonnet_simulation_tools.fit_linear_regression(
        ((fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6)[1:],
        y_data=k[1:],
        plot_graph=True,
        x_label="$\Delta f0_2$ (MHz)",
        y_label="$\Delta f0_1/\Delta f0_2$",
    )

    ########################################################################################################################

    plt.figure(figsize=(8, 6))
    plt.plot(
        ((fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6),
        fit_dictionary["Qr_1"] * 1e-6,
        linestyle="none",
        marker="o",
        label="Qr_1",
    )
    plt.plot(
        ((fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6),
        fit_dictionary["Qc_1"] * 1e-6,
        linestyle="none",
        marker="s",
        label="Qc_1",
    )
    plt.plot(
        ((fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6),
        fit_dictionary["Qr_2"] * 1e-6,
        linestyle="none",
        marker="o",
        label="Qr_2",
    )
    plt.plot(
        ((fit_dictionary["f0_2"] - fit_dictionary["f0_2"][0]) * 1e-6),
        fit_dictionary["Qc_2"] * 1e-6,
        linestyle="none",
        marker="s",
        label="Qc_2",
    )
    plt.xlabel(
        "$\Delta f0_2$ (MHz)",
    )
    plt.ylabel("Q * 1e6")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
