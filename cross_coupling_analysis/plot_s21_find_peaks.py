import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import skrf as rf
import scipy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os


def main():
    directory = r"..\data"

    for file_index, file in enumerate(os.listdir(directory)):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        if ".s2p" in filename:

            network = util.sonnet_simulation_tools.make_renormalised_network(
                data_file_path=file_path, n_ports=2
            )

            peak_indices = scipy.signal.find_peaks(
                abs(network.s_db[:, 0, 1]), prominence=1, height=1
            )[0]

            plt.figure(figsize=(8, 6))
            plt.plot(network.f * 1e-9, network.s_db[:, 0, 1])
            plt.plot(
                network.f[peak_indices] * 1e-9,
                network.s_db[:, 0, 1][peak_indices],
                linestyle="none",
                marker="o",
                fillstyle="none",
                color="r"
            )
            plt.xlabel("Frequency (GHz)")
            plt.ylabel("S21 (dB)")
            plt.show()


if __name__ == "__main__":
    main()
